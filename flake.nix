{
  description = "NixOS configuration for my machines";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    systems.url = "github:nix-systems/x86_64-linux";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    deploy-rs = {
      url = "github:serokell/deploy-rs";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    emacs-overlay = {
      url = "github:/nix-community/emacs-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    ragenix = {
      url = "github:yaxitech/ragenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    blog = {
      url = "gitlab:smaximov/maximov.space";
      inputs.systems.follows = "systems";
    };

    gainz = {
      url = "git+ssh://git@gitlab.com/smaximov/gainz";
      inputs.systems.follows = "systems";
    };

    niri.url = "github:sodiboo/niri-flake";
    stylix.url = "github:danth/stylix";

    nur = {
      url = "github:nix-community/NUR";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      nixos-generators,
      deploy-rs,
      ...
    }@inputs:
    {
      packages.x86_64-linux = {
        # Build with `nix build ~/src/nixos-configuration#digital-ocean-image`
        digital-ocean-image = nixos-generators.nixosGenerate {
          system = "x86_64-linux";
          format = "do";
          modules = [
            # Hide that annoying warning when deploying unrelated machines with deploy-rs:
            { system.stateVersion = "23.05"; }
          ];
        };
      };

      nixosConfigurations =
        with nixpkgs.lib;
        let
          hosts = builtins.attrNames (builtins.readDir ./hosts);

          mkHostName = builtins.replaceStrings [ "." ] [ "-" ];

          mkHost =
            name:
            nixosSystem {
              system = builtins.readFile (./hosts + "/${name}/system");

              modules = [
                ./modules

                { networking.hostName = mkHostName name; }

                (
                  { lib, inputs, ... }:
                  {
                    system.configurationRevision = lib.mkIf (inputs.self ? rev) inputs.self.rev;
                  }
                )

                (./hosts + "/${name}/configuration.nix")
              ];

              specialArgs = {
                inherit inputs;
                host = name;
              };
            };
        in
        genAttrs hosts mkHost;

      deploy.nodes."place-holder" = {
        hostname = "place-holder.ru";

        profiles.system = {
          sshUser = "root";

          path = deploy-rs.lib.x86_64-linux.activate.nixos self.nixosConfigurations."place-holder.ru";
        };
      };

      # This is highly advised, and will prevent many possible mistakes
      checks = builtins.mapAttrs (system: deployLib: deployLib.deployChecks self.deploy) deploy-rs.lib;
    };
}
