{
  lib,
  config,
  inputs,
  ...
}:
let
  inherit (lib)
    mkOption
    mkEnableOption
    mkDefault
    mkIf
    types
    ;

  cfg = config.custom.services.v2ray;
in
{
  options.custom.services.v2ray = {
    enable = mkEnableOption "v2ray";

    secret = mkOption {
      type = types.submodule (
        { config, ... }:
        {
          freeformType = types.anything;

          options = {
            file = mkOption { type = types.path; };
            path = mkOption { type = types.str; };
          };
        }
      );
    };
  };

  config = mkIf cfg.enable {
    services.v2ray = {
      enable = true;
      configFile = cfg.secret.path;
    };

    systemd.services.v2ray = {
      restartTriggers = [ cfg.secret.file ];

      # Delete service config once https://github.com/NixOS/nixpkgs/pull/338404 is merged
      serviceConfig = {
        LoadCredential = "config.json:${cfg.secret.path}";
        ExecStart = [
          ""
          "${config.services.v2ray.package}/bin/v2ray run -config %d/config.json"
        ];
      };
    };
  };
}
