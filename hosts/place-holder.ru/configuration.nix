{
  config,
  pkgs,
  inputs,
  host,
  ...
}:

{
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    inputs.ragenix.nixosModules.default
    inputs.gainz.nixosModules.default
  ];

  nixpkgs.overlays = [ inputs.gainz.overlays.default ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/vda"; # or "nodev" for efi only

  age = {
    secrets.syncthing-basic-auth = {
      file = ../../secrets/syncthing-basic-auth.age;
      mode = "770";
      owner = "nginx";
      group = "nginx";
    };

    secrets.gainz =
      let
        cfg = config.services.gainz;
      in
      {
        file = ../../secrets/gainz.age;
        # Restrict read-only access to the decrypted file to the service user:
        owner = cfg.user;
        group = cfg.group;
        mode = "440";
      };

    secrets.v2ray = {
      file = ../../secrets/v2ray/server.age;
      mode = "440";
    };
  };

  # Static networking setup since `dhcpcd` doesn't work for this hosting provider:
  networking.interfaces.ens3 = {
    useDHCP = false;
    ipv4.addresses = [
      {
        address = "94.131.100.112";
        prefixLength = 24;
      }
    ];
  };
  networking.defaultGateway = "94.131.100.1";
  networking.nameservers = [
    "1.1.1.1"
    "8.8.8.8"
    "8.8.4.4"
  ];

  networking.firewall.allowedTCPPorts = [
    # Open HTTP and HTTPS ports:
    80
    443
    # v2ray
    20088
  ];

  networking.firewall.allowedTCPPortRanges = [
    # Ports for TCP forwarding via SSH tunnel
    {
      from = 8800;
      to = 8900;
    }
  ];

  # Enable the OpenSSH daemon, disable logging in via password:
  services.openssh = {
    enable = true;
    settings = {
      PasswordAuthentication = false;
      # Force TCP port forwarding to bind to the wildcard address
      # so it would be accessible for remote hosts.
      GatewayPorts = "yes";
    };
  };

  # We disabled password authentication, but it's still a good
  # idea to block attempts connections that try to log in with
  # a password anyways:
  services.sshguard.enable = true;

  # Add an authorized public key to log in via SSH with:
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPhxY6BbX7d2vtqbr4rbIDAMuL3kuRN6pEU3qHQxWqgN smaximov @ home"
  ];

  services.syncthing = {
    enable = true;
    openDefaultPorts = true;
    settings = {
      gui = {
        theme = "dark";
        insecureSkipHostcheck = true;
      };

      devices = {
        phone = {
          id = "SEKPMHQ-2VLWFRD-J263ZHN-SGOZCBU-GPSQ5BE-UGR4SFO-AFGPNRP-VUBEZQL";
          name = "realme GT Master Edition";
          introducer = true;
          autoAcceptFolders = true;
        };
        desktop = {
          id = "6N3SB3Y-BVVACBR-4UFINQV-F2DZ4K5-BWB63MM-NXB6WFS-KHGQJOP-45OF7AX";
          name = "home-desktop";
          introducer = true;
          autoAcceptFolders = true;
        };
      };
    };
    # Opt out of declarative folders setup: persist folders added via the web
    # interface.
    overrideFolders = false;
  };

  services.nginx = {
    enable = true;

    # Enable recommended Nginx settings:
    recommendedTlsSettings = true;
    recommendedOptimisation = true;
    recommendedGzipSettings = true;
    recommendedProxySettings = true;

    virtualHosts.${host} =
      let
        blog = inputs.blog.packages.${pkgs.system}.default;
      in
      {
        default = true;
        forceSSL = true;
        enableACME = true;
        root = "${blog}/public";
      };

    virtualHosts."sync.${host}" =
      let
        syncthing-address = config.services.syncthing.guiAddress;
      in
      {
        forceSSL = true;
        enableACME = true;
        locations."/" = {
          basicAuthFile = config.age.secrets.syncthing-basic-auth.path;
          proxyPass = "http://${syncthing-address}";
        };
      };

    virtualHosts."show.${host}" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://127.0.0.1:8888";
        proxyWebsockets = true;
      };
    };
  };

  security.acme = {
    # Accept the CA's terms of service:
    acceptTerms = true;
    # Firefox Relay's masked email that forwards mail to my account:
    defaults.email = "aalm2a0ff@mozmail.com";
  };

  services.gainz = {
    enable = true;
    host = "gainz.${host}";
    secret-env-file = config.age.secrets.gainz.path;

    postgres.enable = true;
    nginx.enable = true;
  };

  custom.services.v2ray = {
    enable = true;
    secret = config.age.secrets.v2ray;
  };

  system.stateVersion = "22.11"; # Did you read the comment?
}
