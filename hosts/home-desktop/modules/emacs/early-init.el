;;;; -*- lexical-binding: t; -*-

;; Don't show splash screen at startup
(setq inhibit-splash-screen t)
