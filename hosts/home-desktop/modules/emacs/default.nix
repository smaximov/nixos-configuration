{ pkgs, config, ... }:

let
  emacs = pkgs.emacsWithPackagesFromUsePackage {
    # Your Emacs config file. Org mode babel files are also
    # supported.
    # NB: Config files cannot contain unicode characters, since
    #     they're being parsed in nix, which lacks unicode
    #     support.
    # config = ./emacs.org;
    config = ./init.el;

    # Whether to include your config as a default init file.
    # If being bool, the value of config is used.
    # Its value can also be a derivation like this if you want to do some
    # substitution:
    #   defaultInitFile = pkgs.substituteAll {
    #     name = "default.el";
    #     src = ./emacs.el;
    #     inherit (config.xdg) configHome dataHome;
    #   };
    defaultInitFile = pkgs.substituteAll {
      name = "default.el";
      src = ./init.el;
      inherit (config.xdg) configHome dataHome;
    };

    package = pkgs.emacs-pgtk;

    # Optionally provide extra packages not in the configuration file.
    extraEmacsPackages =
      epkgs:
      let
        theme = epkgs.trivialBuild {
          pname = "base16-stylix-theme";
          version = "0.1.0";
          packageRequires = [ epkgs.base16-theme ];
          src =
            with config.lib.stylix.colors.withHashtag;
            pkgs.writeText "base16-stylix-theme.el" ''
              (require 'base16-theme)

              (defvar base16-stylix-theme-colors
                '(:base00 "${base00}"
                  :base01 "${base01}"
                  :base02 "${base02}"
                  :base03 "${base03}"
                  :base04 "${base04}"
                  :base05 "${base05}"
                  :base06 "${base06}"
                  :base07 "${base07}"
                  :base08 "${base08}"
                  :base09 "${base09}"
                  :base0A "${base0A}"
                  :base0B "${base0B}"
                  :base0C "${base0C}"
                  :base0D "${base0D}"
                  :base0E "${base0E}"
                  :base0F "${base0F}")
                "All colors for Base16 stylix are defined here.")

              ;; Define the theme
              (deftheme base16-stylix)

              ;; Add all the faces to the theme
              (base16-theme-define 'base16-stylix base16-stylix-theme-colors)

              ;; Mark the theme as provided
              (provide-theme 'base16-stylix)

              ;; Add path to theme to theme-path
              (add-to-list 'custom-theme-load-path
                  (file-name-directory
                      (file-truename load-file-name)))

              (provide 'base16-stylix-theme)
            '';
        };
      in
      [
        theme

        (epkgs.treesit-grammars.with-grammars (
          grammars: with grammars; [
            tree-sitter-bash
            tree-sitter-css
            tree-sitter-dockerfile
            tree-sitter-elisp
            tree-sitter-elixir
            tree-sitter-heex
            tree-sitter-html
            tree-sitter-javascript
            tree-sitter-json
            tree-sitter-nix
            tree-sitter-ruby
            tree-sitter-rust
            tree-sitter-toml
            tree-sitter-tsx
            tree-sitter-typescript
            tree-sitter-yaml
            tree-sitter-nu
          ]
        ))
      ];
  };
in
{
  home.packages = [ emacs ];
  xdg.configFile."emacs/early-init.el".source = ./early-init.el;
}
