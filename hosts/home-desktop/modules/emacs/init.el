;;;; -*- lexical-binding: t; -*-

;;; General configuration

;; Tweak GC
(setf gc-cons-threshold 50000000)

;; When emacs is built without X frontend some features like
;; toolbar or scrollbar are unavailable; that causes
;; initialization to fail. To avoid that, check if the feature is present first
(seq-each (lambda (mode)
            (when (fboundp mode)
              (funcall mode 0)))
          '(tool-bar-mode scroll-bar-mode menu-bar-mode))

;; Indent using spaces
(setq-default indent-tabs-mode nil)

;; Backup and autosave files setttings
(setf backup-directory-alist `(("." . ,temporary-file-directory))
      create-lockfiles nil)

;; Display current column position of the cursor
(add-hook 'after-init-hook #'column-number-mode)

;; Track recent files
(add-hook 'after-init-hook #'recentf-mode)

(eval-and-compile
  (customize-set-variable
   'package-archives '(("org" . "https://orgmode.org/elpa/")
                       ("melpa" . "https://melpa.org/packages/")
                       ("gnu" . "https://elpa.gnu.org/packages/")
                       ("melpa-stable" . "https://stable.melpa.org/packages/")))
  (package-initialize)
  (unless (package-installed-p 'leaf)
    (package-refresh-contents)
    (package-install 'leaf))

  (leaf leaf-keywords
    :ensure t
    :doc "Additional leaf.el keywords for external packages"
    :url "https://github.com/conao3/leaf-keywords.el"
    :init
    (leaf blackout
      :ensure t
      :doc "The easy way to clean up your Emacs mode lighters"
      :url "https://github.com/radian-software/blackout")
    :config
    ;; initialize leaf-keywords.el
    (leaf-keywords-init)))

;; Follow links to VCS-controlled source files
(setq vc-follow-symlinks t)

;; Increase/decrease block indentation without hands leaving the home row
(define-key indent-rigidly-map (kbd "C-f") #'indent-rigidly-right)
(define-key indent-rigidly-map (kbd "C-b") #'indent-rigidly-left)

;; Handle whitespace
(add-hook 'before-save-hook #'delete-trailing-whitespace)
(require 'hl-line)
(global-hl-line-mode)

;; Visual line mode
(setq visual-line-fringe-indicators
      (or (cdr (assoc 'continuation fringe-indicator-alist))
          visual-line-fringe-indicators))
(add-hook 'compilation-mode-hook #'visual-line-mode)
(add-hook 'flycheck-error-list-mode-hook #'visual-line-mode)

;; Use UTF-8 by default
(prefer-coding-system 'utf-8)

;; Cyrillic support
(setq default-input-method 'russian-computer)

(defun delete-whitespace-till-next-word ()
  "Delete all white space from point to the next word."
  (interactive)
  (if (looking-at "[ \t\n]+")
      (replace-match "")
    (ding)))

(global-set-key (kbd "M-\\") #'delete-whitespace-till-next-word)

(global-set-key (kbd "C-,") (lambda () (interactive) (other-window -1)))
(global-set-key (kbd "C-.") (lambda () (interactive) (other-window +1)))

(require 'epa)

;; Use minibuffer to ask for the GPG passphrase
(setq epa-pinentry-mode 'loopback)

(defun create-lex-comparator-with-ext (ext)
  "Create pathnames comparator which prefers pathnames which have EXT extension."
  (lambda (a b)
    (let ((a-base (file-name-sans-extension a))
          (b-base (file-name-sans-extension b)))
      (cond
       ((string-lessp a-base b-base)
        t)
       ((string-lessp b-base a-base)
        nil)
       (t
        (string-equal (file-name-extension a) ext))))))

(setf auth-sources (sort auth-sources (create-lex-comparator-with-ext "gpg")))

(defun auto-insert-save-excursion-advice (original-fun &rest args)
  "Preserve buffer position when calling `auto-insert'."
  (if (= (point) (point-min))
      (apply original-fun args)
    (save-excursion (apply original-fun args))))

;; Force windows to open horizontally (side-by-side)
(setq split-height-threshold nil
      split-width-threshold 120)

(advice-add 'auto-insert :around #'auto-insert-save-excursion-advice)

;; Measure Emacs init time
(add-hook 'emacs-startup-hook
          (lambda () (message (emacs-init-time "Init time: %s seconds"))))

(set-face-attribute 'default nil :font "Iosevka-10")

;; Treat camel-cased words as individual words.
(add-hook 'prog-mode-hook #'subword-mode)
;; Don't assume sentences end with two spaces after a period.
(setq sentence-end-double-space nil)
;; Show matching parens
(show-paren-mode +1)
;; Limit files to 80 columns. Controversial, I know.
(setq-default fill-column 80)
;; Handle very long lines without hurting emacs
(global-so-long-mode +1)

;; Stylix theme
(require 'base16-stylix-theme)
(setq base16-theme-256-color-source 'colors)
(load-theme 'base16-stylix t)

;;; Packages
(leaf leaf-tree
  :ensure t
  :doc "Interactive side-bar feature for leaf based init.el"
  :url "https://github.com/conao3/leaf-tree.el")

(leaf projectile
  :ensure t
  :doc "Project Interaction Library for Emacs"
  :url "https://github.com/bbatsov/projectile"
  :hook (after-init-hook . (lambda () (projectile-mode +1)))
  :custom (projectile-mode-line-prefix . " P")
  :bind (projectile-mode-map
         ("C-x p" . projectile-command-map)))

(leaf magit
  :ensure t
  :doc "A Git porcelain inside Emacs"
  :url "https://github.com/magit/magit"
  :preface
  (defconst nameless:magit-dotjira ".jira")

  (defun nameless:magit-insert-current-jira-issue-number ()
    "Get current current JIRA issue number from branch name and insert it at point."
    (interactive)

    (if (or current-prefix-arg
            (> (minibuffer-depth) 0))
        (nameless:magit-insert-current-jira-issue-prefix)
      (let ((branch (magit-get-current-branch)))
        (if branch
            (save-match-data
              (if (string-match "^\\([A-Z]+\\)[-_]\\([[:digit:]]+\\)" branch)
                  (insert (format "%s-%s "
                                  (match-string-no-properties 1 branch)
                                  (match-string-no-properties 2 branch)))
                (nameless:magit-insert-current-jira-issue-prefix)))
          (user-error "This branch doesn't reference a JIRA issue")))))

  (defun nameless:magit-insert-current-jira-issue-prefix ()
    (interactive)
    (when-let* ((root (locate-dominating-file default-directory
                                              nameless:magit-dotjira))
                (dotjira (concat root nameless:magit-dotjira)))
      (if (file-exists-p dotjira)
          (insert (format "%s-"
                          (string-trim-right
                           (with-temp-buffer
                             (insert-file-contents dotjira)
                             (buffer-string)))))
        (user-error "Unable to infer JIRA issue prefix"))))
  :bind (("C-x g" . magit-status)
         ("C-x C-g" . magit-dispatch-popup)
         ("C-x j" . nameless:magit-insert-current-jira-issue-number))
  :custom ((git-commit-summary-max-length . 72))
  :hook ((git-commit-mode-hook . (lambda ()
                                   (setq fill-column 72)))))

(leaf flycheck
  :ensure t
  :doc "On the fly syntax checking for GNU Emacs "
  :url "https://github.com/flycheck/flycheck"
  :blackout t
  :custom (flycheck-disabled-checkers . '(emacs-lisp-checkdoc))
  :hook (after-init-hook . global-flycheck-mode)
  :bind (("M-n" . flycheck-next-error)
         ("M-p" . flycheck-previous-error))

  :init
  (leaf flycheck-posframe
    :ensure t
    :after flycheck
    :doc "Show flycheck errors via posframe.el"
    :url "https://github.com/alexmurray/flycheck-posframe"
    :hook (flycheck-mode-hook . flycheck-posframe-mode)
    :config (flycheck-posframe-configure-pretty-defaults)))

(leaf direnv
  :ensure t
  :doc "Direnv integration for Emacs"
  :url "https://github.com/wbolster/emacs-direnv"
  :hook (after-init-hook . direnv-mode))

(leaf eldoc
  :ensure t
  :doc "Show function arglist or variable docstring in echo area"
  :blackout t)

(leaf telephone-line
  :ensure t
  :doc "A new implementation of Powerline for Emacs"
  :url "https://github.com/dbordak/telephone-line"
  :custom ((telephone-line-primary-left-separator . 'telephone-line-cubed-left)
           (telephone-line-secondary-left-separator . 'telephone-line-cubed-hollow-left)
           (telephone-line-primary-right-separator . 'telephone-line-cubed-right)
           (telephone-line-secondary-right-separator . 'telephone-line-cubed-hollow-right))
  :hook (after-init-hook . (lambda () (telephone-line-mode 1))))

(leaf racket-mode
  :ensure t
  :doc "Emacs major and minor modes for Racket: edit, REPL, check-syntax, debug, profile, and more"
  :url "https://github.com/greghendershott/racket-mode"
  :hook (racket-mode-hook . racket-xp-mode))

(leaf nix-mode
  :ensure t
  :doc "An Emacs major mode for editing Nix expressions"
  :url "https://github.com/NixOS/nix-mode"
  :mode "\\.nix\\'"
  :hook (before-save-hook . nix-format-before-save))

(leaf elixir-ts-mode
  :ensure nil                           ; builtin
  :doc "Elixir mode using Treesitter for fontification, navigation and indentation"
  :url "https://github.com/wkirschbaum/elixir-ts-mode"
  :require direnv lsp-mode
  :mode "\\.exs?\\'"
  :hook
  (elixir-ts-mode-hook . lsp-deferred)
  (elixir-ts-mode-hook . prettify-symbols-mode)
  (elixir-ts-mode-hook . (lambda ()
                           (push '(">=" . ?\u2265) prettify-symbols-alist)
                           (push '("<=" . ?\u2264) prettify-symbols-alist)
                           (push '("!=" . ?\u2260) prettify-symbols-alist)
                           (push '("==" . ?\u2A75) prettify-symbols-alist)
                           (push '("=~" . ?\u2245) prettify-symbols-alist)
                           (push '("<-" . ?\u2190) prettify-symbols-alist)
                           (push '("->" . ?\u2192) prettify-symbols-alist)
                           (push '("<-" . ?\u2190) prettify-symbols-alist)
                           (push '("|>" . ?\u25B7) prettify-symbols-alist)))
  :custom
  (lsp-elixir-local-server-command . "elixir-ls")
  (lsp-elixir-server-command . '("elixir-ls"))
  :config
  (leaf exunit
    :ensure t
    :doc "Emacs ExUnit test runner"
    :url "https://github.com/ananthakumaran/exunit.el"
    :hook (elixir-ts-mode-hook . exunit-mode)
    :custom (exunit-prefer-async-tests . t)))

(leaf lsp-mode
  :ensure t
  :doc "Emacs client/library for the Language Server Protocol"
  :url "https://github.com/emacs-lsp/lsp-mode"
  :preface
  (defun nameless:lsp-mode-setup-completion ()
    (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
          '(orderless flex)))
  :commands lsp
  :hook (lsp-completion-mode . nameless:lsp-mode-setup-completion)
  :custom
  (lsp-rust-analyzer-cargo-watch-command . "clippy")
  (lsp-eldoc-render-all . t)
  (lsp-eldoc-enable-hover . t)
  (lsp-idle-delay . 0.6)
  (lsp-rust-analyzer-server-display-inlay-hints . t)
  (lsp-completion-provider . :none)
  :config
  (setf lsp-prefer-flymake nil)

  (leaf lsp-ui
    :ensure t
    :doc "UI integrations for lsp-mode"
    :url "https://github.com/emacs-lsp/lsp-ui"
    :hook (lsp-mode-hook . lsp-ui-mode)
    :bind (([remap xref-find-definitions] . lsp-ui-peek-find-definitions)
           ([remap xref-find-references] . lsp-ui-peek-find-references))))

(leaf lua-mode
  :ensure t
  :doc "Emacs major mode for editing Lua"
  :url "https://github.com/immerrr/lua-mode"
  :custom
  ((lua-indent-level . 2)
   (lua-indent-string-contents . t)))

(leaf yaml-mode
  :ensure t
  :doc "The emacs major mode for editing files in the YAML data serialization format"
  :url "https://github.com/yoshiki/yaml-mode")

(leaf apheleia
  :ensure t
  :doc "Run code formatter on buffer contents without moving point, using RCS patches and dynamic programming"
  :url "https://github.com/radian-software/apheleia"
  :require apheleia
  :preface
  ;; Workaround for apheleia not applying `.formatter.exs` config while formatting Elixir files;
  ;; See https://github.com/radian-software/apheleia/issues/30 for details.
  (defun locate-dominating-formatter-config-for-mix (orig-fn command &optional callback)
    (if (string-equal (car command) "mix-format")
        (let ((formatter-dir (and buffer-file-name (locate-dominating-file buffer-file-name ".formatter.exs"))))
          (if formatter-dir
              (let ((default-directory formatter-dir))
                (funcall orig-fn command callback))
            (funcall orig-fn command callback)))
      (funcall orig-fn command callback)))
  :advice (:around apheleia-format-buffer locate-dominating-formatter-config-for-mix)
  :hook (after-init-hook . apheleia-global-mode))

(leaf beacon
  :ensure t
  :doc "A light that follows your cursor around so you don't lose it"
  :url "https://github.com/Malabarba/beacon"
  :init
  (beacon-mode +1))

(leaf vertico
  :ensure t
  :doc "VERTical Interactive COmpletion"
  :url "https://github.com/minad/vertico"
  :tag "completion" "minibuffer"
  :init
  (vertico-mode +1))

(leaf orderless
  :ensure t
  :doc "Emacs completion style that matches multiple regexps in any order"
  :url "https://github.com/oantolin/orderless"
  :tag "completion" "minibuffer"
  :init
  (setq completion-styles '(orderless basic)
        completion-category-overrides '((file (styles partial-completion)))))

(leaf consult
  :ensure t
  :doc "Consulting completing-read"
  :url "https://github.com/minad/consult"
  :tag "completion" "minibuffer"
  :bind (([remap switch-to-buffer] . consult-buffer)                           ; C-x b
         ([remap switch-to-buffer-other-window] . consult-buffer-other-window) ; C-x 4 b
         ([remap switch-to-buffer-other-frame] . consult-buffer-other-frame)   ; C-x 5 b
         ([remap yank-pop] . consult-yank-pop)                                 ; M-y
         ([remap repeat-complex-command] . consult-complex-command)            ; C-x M-:
         ([remap isearch-forward] . consult-line)                              ; C-s
         ("C-c i" . consult-imenu)
         ("C-c g" . consult-ripgrep))
  :init
  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)

  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref))

(leaf embark
  :ensure t
  :doc "Emacs Mini-Buffer Actions Rooted in Keymaps"
  :url "https://github.com/oantolin/embark"
  :tag "minibuffer"
  :bind (([remap describe-bindings] . embark-bindings)    ; C-h b
         ("M-o" . embark-act))

  :init
  ;; Consult users will also want the embark-consult package.
  (leaf embark-consult
    :tag "minibuffer"
    :ensure t ; only need to install it, embark loads it after consult if found
    :hook
    (embark-collect-mode . consult-preview-at-point-mode)))

(leaf marginalia
  :ensure t
  :doc "Marginalia in the minibuffer"
  :url "https://github.com/minad/marginalia"
  :tag "minibuffer"
  :init
  (marginalia-mode +1))

(leaf savehist
  :ensure nil                           ; builtin
  :doc "Save minibuffer history"
  :tag "builtin"
  :init
  (savehist-mode +1))

(leaf corfu
  :ensure t
  :doc "COmpletion in Region FUnction"
  :url "https://github.com/minad/corfu"
  :tag "completion"
  :preface
  (defun corfu-enable-always-in-minibuffer ()
    "Enable Corfu in the minibuffer if Vertico/Mct are not active."
    (unless (or (bound-and-true-p mct--active)
                (bound-and-true-p vertico--input)
                (eq (current-local-map) read-passwd-map))
      (setq-local corfu-echo-delay nil ;; Disable automatic echo and popup
                  corfu-popupinfo-delay nil)
      (corfu-mode 1)))
  :custom
  (corfu-auto . t)                       ; Enable auto completion
  (corfu-separator . ?\s)                ; Orderless field separator
  (corfu-quit-at-boundary . t)           ; Quit at completion boundary
  (corfu-quit-no-match . t)              ; Quit if there is no match
  (corfu-preselect . 'valid)             ; Preselect the prompt
  (corfu-popupinfo-delay . '(0.5 . 0.3)) ; Info popup delay: '(initial-delay . subsequent-delay)
  :init
  (global-corfu-mode +1)
  (corfu-popupinfo-mode +1)
  (corfu-history-mode +1)
  (add-hook 'minibuffer-setup-hook #'corfu-enable-always-in-minibuffer 1)

  (leaf kind-icon
    :ensure t
    :doc "Completion kind text/icon prefix labelling for emacs in-region completion "
    :url "https://github.com/jdtsmith/kind-icon"
    :tag "completion"
    :after corfu
    :custom
    (kind-icon-default-face . 'corfu-default) ; to compute blended backgrounds correctly
    :config
    (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

  (leaf cape
    :ensure t
    :doc "Completion At Point Extensions"
    :url "https://github.com/minad/cape"
    :tag "completion"
    :after corfu lsp-mode
    :advice
    ;; Work around for when lsp+corfu hangs because lsp-completion-at-point doesn't
    ;; handle interrupts correctly. See https://github.com/minad/corfu/issues/188#issuecomment-1148658471
    ;; for details.
    (:around lsp-completion-at-point cape-wrap-noninterruptible)))

(leaf haskell-mode
  :ensure t
  :require direnv lsp
  :doc "Emacs mode for Haskell"
  :url "https://github.com/haskell/haskell-mode")

(leaf lsp-haskell
  :ensure t
  :doc "lsp-mode ❤️ haskell"
  :url "https://github.com/emacs-lsp/lsp-haskell"
  :after haskell lsp-mode
  :hook
  (haskell-mode-hook . lsp-deferred)
  (haskell-literate-mode-hook . lsp-deferred)
  :custom
  (lsp-haskell-server-path . "haskell-language-server"))

(leaf yuck-mode
  :ensure t
  :doc "A minimal Emacs major mode for editing yuck config files."
  :url "https://github.com/mmcjimsey26/yuck-mode")

(leaf nushell-ts-mode
  :ensure t
  :doc "Emacs tree-sitter support for Nushell"
  :url "https://github.com/herbertjones/nushell-ts-mode"
  :mode "\\.nu\\'")

(leaf zig-mode
  :ensure t
  :doc "Zig mode for Emacs"
  :url "https://github.com/ziglang/zig-mode"
  :mode "\\.zig\\'")

;; A few more useful configurations...
(leaf emacs
  :ensure nil                           ; builtin
  :custom
  (completion-cycle-threshold . nil)  ; Disable TAB cycle if there are only few candidates

  ;; Emacs 28: Hide commands in M-x which do not apply to the current mode.
  ;; Corfu commands are hidden, since they are not supposed to be used via M-x.
  (read-extended-command-predicate . #'command-completion-default-include-p)

  ;; Enable indentation+completion using the TAB key.
  ;; `completion-at-point' is often bound to M-TAB.
  (tab-always-indent . 'complete)

  ;; Alist mapping file-specified mode to actual mode.
  (major-mode-remap-alist . '((css-mode . css-ts-mode)
                              (dockerfile-mode . dockerfile-ts-mode)
                              (html-mode . html-ts-mode)
                              (js-mode . js-ts-mode)
                              (js-jsx-mode . tsx-ts-mode)
                              (ruby-mode . ruby-ts-mode)
                              (rust-mode . rust-ts-mode)
                              (typescript-mode . typescript-ts-mode)
                              (yaml-mode . yaml-ts-mode))))
