{ config, lib, ... }:

let
  inherit (lib)
    mkIf
    mkEnableOption
    ;

  cfg = config.configs.nushell;
in
{
  options.configs.nushell = {
    enable = mkEnableOption "nushell";
  };

  config = mkIf cfg.enable {
    stylix.targets.nushell.enable = true;

    programs.nushell = {
      enable = true;
      configFile.source = ./config.nu;
      envFile.source = ./env.nu;
    };
  };
}
