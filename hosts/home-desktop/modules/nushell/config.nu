# Nushell Config File
#
# version = "0.101.0"
#
# A `config.nu` file is used to override default Nushell settings,
# define (or import) custom commands, or run any other startup tasks.
# See https://www.nushell.sh/book/configuration.html
#
# Nushell sets "sensible defaults" for most configuration settings, so
# the user's `config.nu` only needs to override these defaults if
# desired.
#
# This file serves as simple "in-shell" documentation for these
# settings, or you can view a more complete discussion online at:
# https://nushell.sh/book/configuration
#
# You can pretty-print and page this file using:
# config nu --doc | nu-highlight | less -R

# $env.config
# -----------
# The $env.config environment variable is a record containing most Nushell
# configuration settings. Keep in mind that, as a record, setting it to a
# new record will remove any keys which aren't in the new record. Nushell
# will then automatically merge in the internal defaults for missing keys.
#
# The same holds true for keys in the $env.config which are also records
# or lists.
#
# For this reason, settings are typically changed by updating the value of
# a particular key. Merging a new config record is also possible. See the
# Configuration chapter of the book for more information.

# ------------------------
# History-related settings
# ------------------------
# $env.config.history.*

# file_format (string):  Either "sqlite" or "plaintext". While text-backed history
# is currently the default for historical reasons, "sqlite" is stable and
# provides more advanced history features.
$env.config.history.file_format = "sqlite"

# max_size (int): The maximum number of entries allowed in the history.
# After exceeding this value, the oldest history items will be removed
# as new commands are added.
$env.config.history.max_size = 100_000

# sync_on_enter (bool): Whether the plaintext history file is updated
# each time a command is entered. If set to `false`, the plaintext history
# is only updated/written when the shell exits. This setting has no effect
# for SQLite-backed history.
$env.config.history.sync_on_enter = true

# isolation (bool):
# `true`: New history from other currently-open Nushell sessions is not
# seen when scrolling through the history using PrevHistory (typically
# the Up key) or NextHistory (Down key)
# `false`: All commands entered in other Nushell sessions will be mixed with
# those from the current shell.
# Note: Older history items (from before the current shell was started) are
# always shown.
# This setting only applies to SQLite-backed history
$env.config.history.isolation = true

# ----------------------
# Miscellaneous Settings
# ----------------------

# show_banner (bool): Enable or disable the welcome banner at startup
$env.config.show_banner = false

# rm.always_trash (bool):
# true: rm behaves as if the --trash/-t option is specified
# false: rm behaves as if the --permanent/-p option is specified (default)
# Explicitly calling `rm` with `--trash` or `--permanent` always override this setting
# Note that this feature is dependent on the host OS trashcan support.
$env.config.rm.always_trash = false

# recursion_limit (int): how many times a command can call itself recursively
# before an error will be generated.
$env.config.recursion_limit = 50

# ---------------------------
# Commandline Editor Settings
# ---------------------------

# edit_mode (string) "vi" or "emacs" sets the editing behavior of Reedline
$env.config.edit_mode = "emacs"

# Command that will be used to edit the current line buffer with Ctrl+O.
# If unset, uses $env.VISUAL and then $env.EDITOR
#
# Tip: Set to "editor" to use the default editor on Unix platforms using
#      the Alternatives system or equivalent
# $env.config.buffer_editor = "editor"

# cursor_shape_* (string)
# -----------------------
# The following variables accept a string from the following selections:
# "block", "underscore", "line", "blink_block", "blink_underscore", "blink_line", or "inherit"
# "inherit" skips setting cursor shape and uses the current terminal setting.
# $env.config.cursor_shape.emacs = "inherit"         # Cursor shape in emacs mode
# $env.config.cursor_shape.vi_insert = "block"       # Cursor shape in vi-insert mode
# $env.config.cursor_shape.vi_normal = "underscore"  # Cursor shape in normal vi mode

# --------------------
# Completions Behavior
# --------------------
# $env.config.completions.*
# Apply to the Nushell completion system

# algorithm (string): Either "prefix" or "fuzzy"
$env.config.completions.algorithm = "fuzzy"

# sort (string): One of "smart" or "alphabetical"
# In "smart" mode sort order is based on the "algorithm" setting.
# When using the "prefix" algorithm, results are alphabetically sorted.
# When using the "fuzzy" algorithm, results are sorted based on their fuzzy score.
$env.config.completions.sort = "smart"

# case_sensitive (bool): true/false to enable/disable case-sensitive completions
$env.config.completions.case_sensitive = false

# quick (bool):
# true: auto-select the completion when only one remains
# false: prevents auto-select of the final result
$env.config.completions.quick = true

# partial (bool):
# true: Partially complete up to the best possible match
# false: Do not partially complete
# Partial Example: If a directory contains only files named "forage", "food", and "forest",
#                  then typing "ls " and pressing <Tab> will partially complete the first two
#                  letters, "f" and "o". If the directory also includes a file named "faster",
#                  then only "f" would be partially completed.
$env.config.completions.partial = true

# use_ls_colors (bool): When true, apply LS_COLORS to file/path/directory matches
$env.config.completions.use_ls_colors = true

# --------------------
# External Completions
# --------------------
# completions.external.*: Settings related to completing external commands
# and additional completers

# external.enable (bool)
# true: search for external commands on the Path
# false: disabling might be desired for performance if your path includes
#        directories on a slower filesystem
$env.config.completions.external.enable = true

# max_results (int): Limit the number of external commands retrieved from
# path to this value. Has no effect if `...external.enable` (above) is set to `false`
$env.config.completions.external.max_results = 100

# completer (closure with a |spans| parameter): A command to call for *argument* completions
# to commands (internal or external).
#
# The |spans| parameter is a list of strings representing the tokens (spans)
# on the current commandline. It is always a list of at least two strings - The
# command being completed plus the first argument of that command ("" if no argument has
# been partially typed yet), and additional strings for additional arguments beyond
# the first.
#
# This setting is usually set to a closure which will call a third-party completion system, such
# as Carapace.
#
# Note: The following is an over-simplified completer command that will call Carapace if it
# is installed. Please use the official Carapace completer, which can be generated automatically
# by Carapace itself. See the Carapace documentation for the proper syntax.
# $env.config.completions.external.completer = {|spans|
#   carapace $spans.0 nushell ...$spans | from json
# }

# --------------------
# Terminal Integration
# --------------------
# Nushell can output a number of escape codes to enable advanced features in Terminal Emulators
# that support them. Settings in this section enable or disable these features in Nushell.
# Features aren't supported by your Terminal can be disabled. Features can also be disabled,
#  of course, if there is a conflict between the Nushell and Terminal's implementation.

# use_kitty_protocol (bool):
# A keyboard enhancement protocol supported by the Kitty Terminal. Additional keybindings are
# available when using this protocol in a supported terminal. For example, without this protocol,
# Ctrl+I is interpreted as the Tab Key. With this protocol, Ctrl+I and Tab can be mapped separately.
$env.config.use_kitty_protocol = true

# osc2 (bool):
# When true, the current directory and running command are shown in the terminal tab/window title.
# Also abbreviates the directory name by prepending ~ to the home directory and its subdirectories.
$env.config.shell_integration.osc2 = true

# osc7 (bool):
# Nushell will report the current directory to the terminal using OSC 7. This is useful when
# spawning new tabs in the same directory.
$env.config.shell_integration.osc7 = true

# osc9_9 (bool):
# Enables/Disables OSC 9;9 support, originally a ConEmu terminal feature. This is an
# alternative to OSC 7 which also communicates the current path to the terminal.
$env.config.shell_integration.osc9_9 = false

# osc8 (bool):
# When true, the `ls` command will generate clickable links that can be launched in another
# application by the terminal.
# Note: This setting replaces the now deprecated `ls.clickable_links`
$env.config.shell_integration.osc8 = true

# osc133 (bool):
# true/false to enable/disable OSC 133 support, a set of several escape sequences which
# report the (1) starting location of the prompt, (2) ending location of the prompt,
# (3) starting location of the command output, and (4) the exit code of the command.
# originating with Final Term. These sequences report information regarding the prompt
# location as well as command status to the terminal. This enables advanced features in
# some terminals, including the ability to provide separate background colors for the
# command vs. the output, collapsible output, or keybindings to scroll between prompts.
$env.config.shell_integration.osc133 = true

# osc633 (bool):
# true/false to enable/disable OSC 633, an extension to OSC 133 for Visual Studio Code
$env.config.shell_integration.osc633 = true

# reset_application_mode (bool):
# true/false to enable/disable sending ESC[?1l to the terminal
# This sequence is commonly used to keep cursor key modes in sync between the local
# terminal and a remove SSH host.
$env.config.shell_integration.reset_application_mode = true

# bracketed_paste (bool):
# true/false to enable/disable the bracketed-paste feature, which allows multiple-lines
# to be pasted into Nushell at once without immediate execution. When disabled,
# each pasted line is executed as it is received.
# Note that bracketed paste is not currently supported on the Windows version of
# Nushell.
$env.config.bracketed_paste = true

# use_ansi_coloring ("auto" or bool):
# The default value `"auto"` dynamically determines if ANSI coloring is used.
# It evaluates the following environment variables in decreasingly priority:
# `FORCE_COLOR`, `NO_COLOR`, and `CLICOLOR`.
# - If `FORCE_COLOR` is set, coloring is always enabled.
# - If `NO_COLOR` is set, coloring is disabled.
# - If `CLICOLOR` is set, its value (0 or 1) decides whether coloring is used.
# If none of these are set, it checks whether the standard output is a terminal
# and enables coloring if it is.
# A value of `true` or `false` overrides this behavior, explicitly enabling or
# disabling ANSI coloring in Nushell's internal commands.
# When disabled, built-in commands will only use the default foreground color.
# Note: This setting does not affect the `ansi` command.
$env.config.use_ansi_coloring = "auto"

# ----------------------
# Error Display Settings
# ----------------------

# error_style (string): One of "fancy" or "plain"
# Plain: Display plain-text errors for screen-readers
# Fancy: Display errors using line-drawing characters to point to the span in which the
#        problem occurred.
$env.config.error_style = "fancy"

# display_errors.exit_code (bool):
# true: Display a Nushell error when an external command returns a non-zero exit code
# false: Display only the error information printed by the external command itself
# Note: Core dump errors are always printed; SIGPIPE never triggers an error
$env.config.display_errors.exit_code = false

# display_errors.termination_signal (bool):
# true/false to enable/disable displaying a Nushell error when a child process is
# terminated via any signal
$env.config.display_errors.termination_signal = true

# -------------
# Table Display
# -------------
# footer_mode (string or int):
# Specifies when to display table footers with column names. Allowed values:
# "always"
# "never"
# "auto": When the length of the table would scroll the header past the first line of the terminal
# (int): When the number of table rows meets or exceeds this value
# Note: Does not take into account rows with multiple lines themselves
$env.config.footer_mode = "auto"

# table.*
# table_mode (string):
# One of: "default", "basic", "compact", "compact_double", "heavy", "light", "none", "reinforced",
# "rounded", "thin", "with_love", "psql", "markdown", "dots", "restructured", "ascii_rounded",
# or "basic_compact"
# Can be overridden by passing a table to `| table --theme/-t`
$env.config.table.mode = "default"

# index_mode (string) - One of:
# "never": never show the index column in a table or list
# "always": always show the index column in tables and lists
# "auto": show the column only when there is an explicit "index" column in the table
# Can be overridden by passing a table to `| table --index/-i`
$env.config.table.index_mode = "always"

# show_empty (bool):
# true: show "empty list" or "empty table" when no values exist
# false: display no output when no values exist
$env.config.table.show_empty = true

# padding.left/right (int): The number of spaces to pad around values in each column
$env.config.table.padding.left = 1
$env.config.table.padding.right = 1

# trim.*: The rules that will be used to display content in a table row when it would cause the
#         table to exceed the terminal width.
# methodology (string): One of "wrapping" or "truncating"
# truncating_suffix (string): The text to show at the end of the row to indicate that it has
#                             been truncated. Only valid when `methodology = "truncating"`.
# wrapping_try_keep_words (bool): true to keep words together based on whitespace
#                                 false to allow wrapping in the middle of a word.
#                                 Only valid when `methodology = wrapping`.
$env.config.table.trim = {
  methodology: "wrapping"
  wrapping_try_keep_words: true
}
# or
# $env.config.table.trim = {
#   methodology: "truncating"
#   truncating_suffix: "..."
# }

# header_on_separator (bool):
# true: Displays the column headers as part of the top (or bottom) border of the table
# false: Displays the column header in its own row with a separator below.
$env.config.table.header_on_separator = false

# abbreviated_row_count (int or nothing):
# If set to an int, all tables will be abbreviated to only show the first <n> and last <n> rows
# If set to `null`, all table rows will be displayed
# Can be overridden by passing a table to `| table --abbreviated/-a`
$env.config.table.abbreviated_row_count = null

# footer_inheritance (bool): Footer behavior in nested tables
# true: If a nested table is long enough on its own to display a footer (per `footer_mode` above),
#       then also display the footer for the parent table
# false: Always apply `footer_mode` rules to the parent table
$env.config.table.footer_inheritance = false

# ----------------
# Datetime Display
# ----------------
# datetime_format.* (string or nothing):
# Format strings that will be used for datetime values.
# When set to `null`, the default behavior is to "humanize" the value (e.g., "now" or "a day ago")

# datetime_format.table (string or nothing):
# The format string (or `null`) that will be used to display a datetime value when it appears in a
# structured value such as a table, list, or record.
# $env.config.datetime_format.table = null

# datetime_format.normal (string or nothing):
# The format string (or `null`) that will be used to display a datetime value when it appears as
# a raw value.
$env.config.datetime_format.normal = "%y/%m/%d %I:%M:%S%p"

# ----------------
# Filesize Display
# ----------------
# filesize.unit (string): One of either:
# - A filesize unit: "B", "kB", "KiB", "MB", "MiB", "GB", "GiB", "TB", "TiB", "PB", "PiB", "EB", or "EiB".
# - An automatically scaled unit: "metric" or "binary".
# "metric" will use units with metric (SI) prefixes like kB, MB, or GB.
# "binary" will use units with binary prefixes like KiB, MiB, or GiB.
# Otherwise, setting this to one of the filesize units will use that particular unit when displaying all file sizes.
$env.config.filesize.unit = "binary"

# ---------------------
# Miscellaneous Display
# ---------------------

# render_right_prompt_on_last_line(bool):
# true: When using a multi-line left-prompt, the right-prompt will be displayed on the last line
# false: The right-prompt is displayed on the first line of the left-prompt
$env.config.render_right_prompt_on_last_line = false

# float_precision (int):
# Float values will be rounded to this precision when displaying in structured values such as lists,
# tables, or records.
$env.config.float_precision = 2

# ls.use_ls_colors (bool):
# true: The `ls` command will apply the $env.LS_COLORS standard to filenames
# false: Filenames in the `ls` table will use the color_config for strings
$env.config.ls.use_ls_colors = true

# Hooks
# -----
# $env.config.hooks is a record containing the five different types of Nushell hooks.
# See the Hooks documentation at https://www.nushell.sh/book/hooks for details
#
# Most hooks can accept a string, a closure, or a list containing strings and/or closures.
# The display_output record can only accept a string or a closure, but never a list
#
# WARNING: A malformed display_output hook can suppress all Nushell output to the terminal.
#          It can be reset by assigning an empty string as below:

# Before each prompt is displayed
# $env.config.hooks.pre_prompt = []

# After <enter> is pressed; before the commandline is executed
# $env.config.hooks.pre_execution = []

# When a specified environment variable changes
# $env.config.hooks.env_change = {
#     # Example: Run if the PWD environment is different since the last REPL input
#     PWD: [{|before, after| null }]
# }

# Before Nushell output is displayed in the terminal
$env.config.hooks.display_output = "if (term size).columns >= 100 { table -e } else { table }"
# When a command is not found
# $env.config.hooks.command_not_found = []

# The env_change hook accepts a record with environment variable names as keys, and a list
# of hooks to run when that variable changes
# $env.config.hooks.env_change = {}

# -----------
# Keybindings
# -----------
# keybindings (list): A list of user-defined keybindings
# Nushell/Reedline keybindings can be added or overridden using this setting.
# See https://www.nushell.sh/book/line_editor.html#keybindings for details.

# Example - Add a new Alt+. keybinding to insert the last token used on the previous commandline
# $env.config.keybindings ++= [
#   {
#     name: insert_last_token
#     modifier: alt
#     keycode: char_.
#     mode: [emacs vi_normal vi_insert]
#     event: [
#       { edit: InsertString, value: "!$" }
#       { send: Enter }
#     ]
#   }
# ]

# Example: Override the F1 keybinding with a user-defined help menu (see "Menus" below):
# $env.config.keybindings ++= [
#   {
#     name: help_menu
#     modifier: none
#     keycode: f1
#     mode: [emacs, vi_insert, vi_normal]
#     event: { send: menu name: help_menu }
#   }
# ]

# Make alt+t swap words like it does in Emacs
$env.config.keybindings ++= [
  {
    name: swap_words
    modifier: alt
    keycode: char_t
    mode: [emacs]
    event: { edit: SwapWords }
  }
]

# Make alt+Backspace (interpreted as ctrl+alt+h) delete a word to the left of the cursor like ctrl+w does
$env.config.keybindings ++= [
  {
    name: cut_word_left
    modifier: control_alt
    keycode: char_h
    mode: [emacs]
    event: { edit: BackspaceWord }
  }
]

# $env.config.keybindings (above).
#
# Simple example - Add a new Help menu to the list (note that a similar menu is already
# defined internally):
# $env.config.menus ++= [
#     {
#         name: help_menu
#         only_buffer_difference: true
#         marker: "? "
#         type: {
#             layout: description
#             columns: 4
#             # col_width is an optional value. If missing, the entire screen width is used to
#             # calculate the column width
#             col_width: 20
#             col_padding: 2
#             selection_rows: 4
#             description_rows: 10
#         }
#         style: {
#             text: green
#             selected_text: green_reverse
#             description_text: yellow
#         }
#     }
# ]

# ---------------
# Plugin behavior
# ---------------
# Per-plugin configuration. See https://www.nushell.sh/contributor-book/plugins.html#plugin-configuration
$env.config.plugins = {}

# Plugin garbage collection configuration
# $env.config.plugin_gc.*

# enabled (bool): true/false to enable/disable stopping inactive plugins
$env.config.plugin_gc.default.enabled = true
# stop_after (duration): How long to wait after a plugin is inactive before stopping it
$env.config.plugin_gc.default.stop_after = 10sec
# plugins (record): Alternate garbage collection configuration per-plugin.
$env.config.plugin_gc.plugins = {
  # gstat: {
  #   enabled: false
  # }
}

# -------------------------------------
# Themes/Colors and Syntax Highlighting
# -------------------------------------
# For more information on defining custom themes, see
# https://www.nushell.sh/book/coloring_and_theming.html

# Use and/or contribute to the theme collection at
# https://github.com/nushell/nu_scripts/tree/main/themes

# Values:

# highlight_resolved_externals (bool):
# true: Applies the `color_config.shape_external_resolved` color (below) to external commands
#       which are found (resolved) on the path
# false: Applies the `color_config.shape_external` color to *all* externals simply based on whether
#        or not they would be *parsed* as an external command based on their position.
# Defaults to false for systems with a slower search path
$env.config.highlight_resolved_externals = true

# color_config (record): A record of shapes, types, UI elements, etc. that can be styled (e.g.,
# colorized) in Nushell, either on the commandline itself (shapes) or in output.
#
# Note that this is usually set through a theme provided by a record in a custom command. For
# instance, the standard library contains two "starter" theme commands: "dark-theme" and
# "light-theme". For example:
# use std/config dark-theme
# $env.config.color_config = (dark-theme)
#
# Instead of started themes, we use the configured system-wide theme via the Stylix integration for
# Nushell.

# ------------------------
# `explore` command colors
# ------------------------
# Configure the UI colors of the `explore` command
# Allowed values are the same as for the `color_config` options above.
# Example:
$env.config.explore = {
    status_bar_background: { fg: "#1D1F21", bg: "#C4C9C6" },
    command_bar_text: { fg: "#C4C9C6" },
    highlight: { fg: "black", bg: "yellow" },
    status: {
        error: { fg: "white", bg: "red" },
        warn: {}
        info: {}
    },
    selected_cell: { bg: light_blue },
}

# --------------------------
# Custom aliases and comands
# --------------------------

# Alias `nixos-rebuild` to avoid typing the full path to the flake every time.
alias nixos-rebuild = ^nixos-rebuild --flake path:/home/nameless/src/nixos-configuration --use-remote-sudo

# Alias `nixos-option` to avoid typing the full path to the flake every time.
alias nixos-option = ^nixos-option --flake path:/home/nameless/src/nixos-configuration

# Displays the configuration settings that differ from the default config.
def list-changed-settings [] {
  let defaults = nu -n -c "$env.config = {}; $env.config | reject color_config keybindings menus | to nuon" | from nuon | transpose key default
  let current = $env.config | reject color_config keybindings menus | transpose key current
  $current | merge $defaults | where $it.current != $it.default
}
