{ config, ... }:

{
  programs.direnv = {
    enable = true;

    nix-direnv.enable = true;

    enableBashIntegration = false;
    enableFishIntegration = false;
    enableZshIntegration = false;

    enableNushellIntegration = config.configs.nushell.enable;
  };
}
