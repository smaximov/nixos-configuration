{
  config,
  lib,
  ...
}:

let
  inherit (lib)
    mkIf
    mkEnableOption
    ;

  inherit (config) configs;

  cfg = configs.starship;
in
{
  options.configs.starship = {
    enable = mkEnableOption "starship";
  };

  config = mkIf cfg.enable {
    programs.starship = {
      enable = true;
      enableBashIntegration = false;
      enableFishIntegration = false;
      enableZshIntegration = false;
      enableNushellIntegration = configs.nushell.enable;
      settings = {
        time = {
          disabled = false;
          format = "[🕙 $time]($style)";
        };

        kubernetes.disabled = false;
      };
    };
  };
}
