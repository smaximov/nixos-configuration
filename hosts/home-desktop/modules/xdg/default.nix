{
  xdg = {
    # The `~/.config/mimeapps.list` symlink created by Home Manager gets
    # overriden by a file with the same content on system start, which causes
    # errors in Home Manager activation scripts when rebuilding the system
    # configuration:
    #
    #   Existing file '/home/nameless/.config/mimeapps.list' is in the way of '/[snip]-home-manager-files/.config/mimeapps.list'
    #   Please move the above files and try again or use 'home-manager switch -b backup' to back up existing files automatically.
    #
    # As a workaround here we ask Home Manager to silently override this file
    # back. More details at the link below:
    # https://github.com/nix-community/home-manager/issues/1213#issuecomment-626240819
    configFile."mimeapps.list".force = true;

    enable = true;

    userDirs = {
      enable = true;
      desktop = "$HOME/desktop";
      download = "$HOME/downloads";
      documents = "$HOME/docs";
      templates = "$HOME/templates";
      music = "$HOME/media/music";
      videos = "$HOME/media/videos";
      pictures = "$HOME/media/pictures";
      publicShare = "$HOME/share/public";
    };

    mimeApps = {
      enable = true;

      defaultApplications = {
        "text/html" = "firefox.desktop";
        "video/x-matroska" = "mpv.desktop";
        "x-scheme-handler/mailto" = "thunderbird.desktop";
        "message/rfc822" = "thunderbird.desktop";
      };

      associations.added = {
        "application/pdf" = "firefox.desktop";
        "x-scheme-handler/sms" = "org.gnome.Shell.Extensions.GSConnect.desktop";
        "x-scheme-handler/tel" = "org.gnome.Shell.Extensions.GSConnect.desktop";
        "x-scheme-handler/tg" = "Telegram Desktop.desktop";
      };
    };
  };
}
