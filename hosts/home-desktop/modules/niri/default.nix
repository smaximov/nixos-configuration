{ config, pkgs, ... }:
{
  home.packages = with pkgs; [
    swaylock
    swaynotificationcenter
    wl-clipboard-rs
    swaybg
    swaylock
    wev
    eww
    libnotify
    playerctl
  ];

  stylix.targets = {
    fuzzel.enable = true;
    swaync.enable = true;
  };

  programs.niri.settings =
    let
      proportions = {
        one-third = {
          proportion = 1.0 / 3.0;
        };
        one-half = {
          proportion = 1.0 / 2.0;
        };
        two-thirds = {
          proportion = 2.0 / 3.0;
        };
      };

      corner-radius = value: {
        top-left = value;
        top-right = value;
        bottom-left = value;
        bottom-right = value;
      };
    in
    with config.lib.stylix.colors.withHashtag;
    {
      spawn-at-startup = [
        {
          command = [
            "eww"
            "open"
            "bar"
          ];
        }

        { command = [ "swaync" ]; }

        {
          command = [
            "swaybg"
            "-i"
            config.stylix.image
          ];
        }
      ];

      screenshot-path =
        let
          pictures-dir = builtins.replaceStrings [ "$HOME" ] [ "~" ] config.xdg.userDirs.pictures;
        in
        "${pictures-dir}/screenshots/%Y-%m-%d_%H-%M-%S.png";

      input.mouse.natural-scroll = false;

      input.keyboard = {
        xkb = {
          layout = "us,ru";
          options = "grp:caps_toggle";
        };

        track-layout = "window";
      };

      # Prevent niri from taking over the power button, which makes the PC sleep. Instead, power it off.
      input.power-key-handling.enable = false;

      outputs."DP-1" = {
        mode = {
          width = 2560;
          height = 1440;
          refresh = 164.958;
        };
      };

      outputs."HDMI-A-1" = {
        enable = false;
      };

      layout = {
        gaps = 16;
        struts.top = 36;

        preset-column-widths = builtins.attrValues proportions;

        focus-ring = {
          active.color = base0D;
          inactive.color = base02;
        };

        tab-indicator = {
          position = "bottom";
          hide-when-single-tab = true;
          gap = 7;
        };
      };

      prefer-no-csd = true;

      environment = {
        QT_QPA_PLATFORM = "wayland";
      };

      binds =
        with config.lib.niri.actions;
        let
          sh = spawn "sh" "-c";
        in
        {
          "Mod+Shift+E".action = quit;
          "Mod+Shift+Slash".action = show-hotkey-overlay;

          # Displays a transient notification with information about the currently
          # focused window (title and app ID).
          "Mod+Slash" = {
            action = sh ''notify-send -e -t 5000 "Focused Window" "$(niri msg focused-window)"'';
            hotkey-overlay.title = "Display information about the currently focused window";
          };

          "Mod+Backspace" = {
            cooldown-ms = 500;
            action = close-window;
          };

          "Mod+WheelScrollDown" = {
            cooldown-ms = 150;
            action = focus-workspace-down;
          };
          "Mod+WheelScrollUp" = {
            cooldown-ms = 150;
            action = focus-workspace-up;
          };
          "Mod+WheelScrollRight" = {
            cooldown-ms = 150;
            action = focus-column-right;
          };
          "Mod+WheelScrollLeft" = {
            cooldown-ms = 150;
            action = focus-column-left;
          };

          "Mod+P".action = focus-window-up-or-bottom;
          "Mod+N".action = focus-window-down-or-top;

          "Mod+R".action = switch-preset-column-width;
          "Mod+F".action = maximize-column;
          "Mod+Shift+F".action = fullscreen-window;
          "Mod+C".action = center-column;

          "Mod+L".action.spawn = "swaylock";
          "Mod+T".action.spawn = "rio";
          "Mod+W".action.spawn = "fuzzel";
          "Mod+Return".action.spawn = [
            "swaync-client"
            "-t"
            "-nw"
          ];

          # Floating windows
          "Mod+Escape".action = toggle-window-floating;
          "Mod+Shift+Escape".action = switch-focus-between-floating-and-tiling;

          # Column/tabbed display
          "Mod+Period".action = focus-column-right;
          "Mod+Comma".action = focus-column-left;

          "Mod+Shift+Period".action = move-column-right;
          "Mod+Shift+Comma".action = move-column-left;

          "Mod+Ctrl+Period".action = consume-or-expel-window-right;
          "Mod+Ctrl+Comma".action = consume-or-expel-window-left;

          "Mod+Space" = {
            action = toggle-column-tabbed-display;
            hotkey-overlay.title = "Toggle column tabbed display";
          };

          # Media control
          "XF86AudioPlay" = {
            action.spawn = [
              "playerctl"
              "play-pause"
            ];
            hotkey-overlay.hidden = true;
          };

          "XF86AudioMute" = {
            action.spawn = [
              "wpctl"
              "set-mute"
              "@DEFAULT_AUDIO_SINK@"
              "toggle"
            ];
            hotkey-overlay.hidden = true;
          };

          "XF86AudioRaiseVolume" = {
            action.spawn = [
              "playerctl"
              "volume"
              "0.1+"
            ];
            hotkey-overlay.hidden = true;
          };

          "XF86AudioLowerVolume" = {
            action.spawn = [
              "playerctl"
              "volume"
              "0.1-"
            ];
            hotkey-overlay.hidden = true;
          };

          "XF86AudioPrev" = {
            action.spawn = [
              "playerctl"
              "previous"
            ];
            hotkey-overlay.hidden = true;
          };

          "XF86AudioNext" = {
            action.spawn = [
              "playerctl"
              "next"
            ];
            hotkey-overlay.hidden = true;
          };
        };

      window-rules = [
        {
          matches = [ { app-id = "firefox"; } ];
          default-column-width = proportions.two-thirds;
        }
        {
          matches = [ { app-id = ''^rio$''; } ];
          default-column-width = proportions.one-third;
          default-column-display = "tabbed";
        }
        {
          matches = [ { app-id = "emacs"; } ];
          default-column-width = proportions.two-thirds;
        }
        {
          matches = [ { app-id = ''^org\.gajim\.Gajim$''; } ];
          default-column-width = proportions.two-thirds;
        }
      ];

      layer-rules = [
        # Add a shadow for fuzzel.
        {
          matches = [ { namespace = "^launcher$"; } ];
          shadow.enable = true;
          # Fuzzel defaults to 10px rounder corner
          geometry-corner-radius = corner-radius 10.0;
        }
      ];
    };

  programs.fuzzel = {
    enable = true;
    settings = {
      main.terminal = "${pkgs.rio}/bin/rio";
    };
  };

  xdg.configFile."eww" = {
    source = ./eww;
    recursive = true;
  };

  xdg.configFile."eww/_colors.scss".text = with config.lib.stylix.colors.withHashtag; ''
    $base00: ${base00};
    $base01: ${base01};
    $base02: ${base02};
    $base03: ${base03};
    $base04: ${base04};
    $base05: ${base05};
    $base06: ${base06};
    $base07: ${base07};
    $base08: ${base08};
    $base09: ${base09};
    $base0A: ${base0A};
    $base0B: ${base0B};
    $base0C: ${base0C};
    $base0D: ${base0D};
    $base0E: ${base0E};
    $base0F: ${base0F};
  '';
}
