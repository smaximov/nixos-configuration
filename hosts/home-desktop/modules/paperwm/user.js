// -*- mode: gnome-shell -*-

const Meta = imports.gi.Meta
const Clutter = imports.gi.Clutter
const St = imports.gi.St
const Main = imports.ui.main
const Shell = imports.gi.Shell

// Extension local imports
let Extension, Me, Tiling, Utils, App, Keybindings

const USEFUL_WINDOW_WIDTHS = {
    ONE_THIRD: '38.195%',
    ONE_HALF: '50%',
    TWO_THIRDS: '61.804%',
}

function init() {
    // Runs _only_ once on startup

    // Initialize extension imports here to make gnome-shell-reload work
    Extension = imports.misc.extensionUtils.getCurrentExtension()
    Me = Extension.imports.user
    Tiling = Extension.imports.tiling
    Utils = Extension.imports.utils
    Keybindings = Extension.imports.keybindings
    App = Extension.imports.app

    Tiling.defwinprop({
        wm_class: 'org.telegram.desktop',
        scratch_layer: true,
    })

    Tiling.defwinprop({
        title: 'Picture-in-Picture',
        wm_class: 'firefox',
        scratch_layer: true,
    })

    Tiling.defwinprop({
        wm_class: 'org.wezfurlong.wezterm',
        preferredWidth: USEFUL_WINDOW_WIDTHS.ONE_THIRD
    })

    Tiling.defwinprop({
        wm_class: 'emacs',
        preferredWidth: USEFUL_WINDOW_WIDTHS.TWO_THIRDS
    })
}

function enable() {
    // Runs on extension reloads, eg. when unlocking the session
}

function disable() {
    // Runs on extension reloads eg. when locking the session (`<super>L).
}
