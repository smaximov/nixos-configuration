{ pkgs, lib, ... }:

{
  home.packages = with pkgs.gnomeExtensions; [ paperwm switcher ];

  xdg.configFile."paperwm/user.js".source = ./user.js;

  dconf.settings = {
    "org/gnome/shell/extensions/paperwm" = {
      show-workspace-indicator = false;
    };

    "org/gnome/desktop/interface".enable-hot-corners = false;
    "org/gnome/mutter".edge-tiling = false;
    "org/gnome/desktop/wm/preferences".focus-mode = "click";

    "org/gnome/shell" = {
      enabled-extensions =
        [ "paperwm@paperwm.github.com" "switcher@landau.fi" ];
    };
  };
}
