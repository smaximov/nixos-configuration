{ pkgs, ... }:

{
  programs.tmux = {
    enable = true;
    clock24 = true;
    keyMode = "emacs";
    shortcut = "h";
    sensibleOnTop = true;
    baseIndex = 1;
    terminal = "screen-256color";

    plugins = with pkgs.tmuxPlugins;
      [
        copycat

        # {
        #   plugin = prefix-highlight;
        #   extraConfig = ''
        #     set -g status-right '#{prefix_highlight} | %a %Y-%m-%d %H:%M'
        #   '';
        # }
      ];

    extraConfig = ''
      set -g mouse on

      bind-key -n C-q send-prefix

      bind '"' split-window -c "#{pane_current_path}"
      bind % split-window -h -c "#{pane_current_path}"
    '';
  };
}
