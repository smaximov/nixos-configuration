{ config, lib, ... }:

let
  inherit (lib)
    mkIf
    mkEnableOption
    ;

  inherit (config) configs;

  cfg = configs.carapace;
in
{
  options.configs.carapace = {
    enable = mkEnableOption "carapace";
  };

  config = mkIf cfg.enable {
    programs.carapace = {
      enable = true;
      enableBashIntegration = false;
      enableFishIntegration = false;
      enableZshIntegration = false;
      enableNushellIntegration = configs.nushell.enable;
    };
  };
}
