{
  pkgs,
  config,
  lib,
  ...
}:

let
  inherit (lib)
    mkIf
    mkEnableOption
    ;

  cfg = config.configs.firefox;
in
{
  options.configs.firefox = {
    enable = mkEnableOption "firefox";
  };

  config = mkIf cfg.enable {
    stylix.targets.firefox = {
      enable = true;
      colorTheme.enable = true;
      profileNames = [ "default" ];
    };

    programs.firefox = {
      enable = true;

      policies = {
        DefaultDownloadDirectory = "/tmp";
      };

      profiles.default = {
        id = 0;
        isDefault = true;
        path = "tvqmayx8.default";

        settings = {
          "browser.aboutConfig.showWarning" = false;
          "browser.search.defaultenginename" = "DuckDuckGo";
          "browser.search.order.1" = "DuckDuckGo";

          "extensions.pocket.enabled" = false;
          "sidebar.verticalTabs" = true;
        };

        search = {
          force = true;
          default = "DuckDuckGo";
          order = [ "DuckDuckGo" ];

          engines = {
            "DuckDuckGo" = {
              definedAliases = [ "@d" ];
              iconUpdateURL = "https://duckduckgo.com/favicon.ico";
              updateInterval = 24 * 60 * 60 * 1000; # every day

              urls = [
                {
                  template = "https://duckduckgo.com/";

                  params = [
                    {
                      name = "q";
                      value = "{searchTerms}";
                    }
                  ];
                }
              ];
            };

            "Nix Packages" = {
              definedAliases = [ "@np" ];
              icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";

              urls = [
                {
                  template = "https://search.nixos.org/packages";
                  params = [
                    {
                      name = "query";
                      value = "{searchTerms}";
                    }
                  ];
                }
              ];
            };

            "Nix Options" = {
              definedAliases = [ "@no" ];
              icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";

              urls = [
                {
                  template = "https://search.nixos.org/options";
                  params = [
                    {
                      name = "query";
                      value = "{searchTerms}";
                    }
                  ];
                }
              ];
            };

            "NixOS Wiki" = {
              definedAliases = [ "@nw" ];
              iconUpdateURL = "https://wiki.nixos.org/favicon.png";
              updateInterval = 24 * 60 * 60 * 1000; # every day

              urls = [
                {
                  template = "https://wiki.nixos.org/w/index.php";
                  params = [
                    {
                      name = "search";
                      value = "{searchTerms}";
                    }
                  ];
                }
              ];
            };
          };
        };

        extensions = {
          force = true;

          packages = with pkgs.nur.repos.rycee.firefox-addons; [
            ublock-origin
            darkreader
            sponsorblock
            foxy-gestures
            return-youtube-dislikes
            multi-account-containers
            private-relay
          ];
        };

        containers = { };

        containersForce = true;
      };
    };
  };
}
