{ pkgs, lib, ... }:

{
  home.packages = with pkgs.gnomeExtensions; [ gsconnect media-controls ];

  dconf.settings = {
    "org/gnome/mutter".dynamic-workspaces = true;

    "org/gnome/shell" = {
      disable-user-extensions = false;

      enabled-extensions = [
        "gsconnect@andyholmes.github.io"
        "native-window-placement@gnome-shell-extensions.gcampax.github.com"
        "screenshot-window-sizer@gnome-shell-extensions.gcampax.github.com"
        "user-theme@gnome-shell-extensions.gcampax.github.com"
        "mediacontrols@cliffniff.github.com"
      ];
    };
  };
}
