local wezterm = require 'wezterm'
local act = wezterm.action
local mux = wezterm.mux

-- Show which key table is active in the status area
wezterm.on('update-right-status', function(window, pane)
  local status
  local name = window:active_key_table()

  if name then
    status = 'TABLE :: ' .. name .. '  '
  elseif window:leader_is_active() then
    status = 'LEADER  '
  else
    status = ''
  end

  window:set_right_status(status)
end)


local config = wezterm.config_builder()
config:set_strict_mode(true)

config.check_for_updates = false
config.color_scheme = 'Wombat'
config.exit_behavior = 'Close'
config.font = wezterm.font({ family='Iosevka', weight='Regular' })
config.font_size = 12
config.tab_bar_at_bottom = true
config.hide_mouse_cursor_when_typing = false
-- HACK: see https://github.com/wez/wezterm/issues/5990#issuecomment-2295721814
config.front_end = "WebGpu"

-- Keyboard configuration
-- Send `CTRL+SHIFT+Space` to activate the leader key
config.leader = { key = 'Space', mods = 'CTRL|SHIFT', timeout_milliseconds = 2000 }

config.keys = {
  -- Send `CTRL+SHIFT+r` to reload Wezterm config
  {
    key = 'r',
    mods = 'CTRL|SHIFT',
    action = act.ReloadConfiguration,
  },
  -- Send `a` while the leader key is active to enter
  -- the `activate_pane` mode
  {
    key = 'a',
    mods = 'LEADER',
    action = act.ActivateKeyTable {
      name = 'activate_pane',
      one_shot = false,
      timeout_milliseconds = 2000,
    },
  },
  -- Send `r` while the leader key is active to enter
  -- the `resize_pane` mode
  {
    key = 'r',
    mods = 'LEADER',
    action = act.ActivateKeyTable {
      name = 'resize_pane',
      one_shot = false,
    },
  },
  { key = 'L', mods = 'CTRL|SHIFT', action = act.ShowDebugOverlay },
}

config.key_tables = {
  -- Defines the keys that are active in our activate-pane mode.
  -- Since we're likely to want to make multiple adjustments,
  -- we made the activation one_shot=false. We therefore need
  -- to define a key assignment for getting out of this mode.
  -- 'activate_pane' here corresponds to the name="activate_pane" in
  -- the key assignments above.
  activate_pane = {
    { key = 'b', action = act.ActivatePaneDirection 'Left' },
    { key = 'p', action = act.ActivatePaneDirection 'Up' },
    { key = 'f', action = act.ActivatePaneDirection 'Right' },
    { key = 'n', action = act.ActivatePaneDirection 'Down' },
    -- Cancel the mode by pressing `Escape`, 'Space', 'Enter' or `Ctrl+G`
    { key = 'Escape', action = 'PopKeyTable' },
    { key = 'Space', action = 'PopKeyTable' },
    { key = 'Enter', action = 'PopKeyTable' },
    { key = 'g', mods = 'CTRL', action = 'PopKeyTable' },
  },

  -- Defines the keys that are active in our resize-pane mode.
  -- Since we're likely to want to make multiple adjustments,
  -- we made the activation one_shot=false. We therefore need
  -- to define a key assignment for getting out of this mode.
  -- `resize_pane` here corresponds to the name="resize_pane" in
  -- the key assignments above.
  resize_pane = {
    { key = 'b', action = act.AdjustPaneSize { 'Left', 1 } },
    { key = 'p', action = act.AdjustPaneSize { 'Up', 1 } },
    { key = 'f', action = act.AdjustPaneSize { 'Right', 1 } },
    { key = 'n', action = act.AdjustPaneSize { 'Down', 1 } },
    -- Cancel the mode by pressing `Escape`, 'Space', 'Enter' or `Ctrl+G`
    { key = 'Escape', action = 'PopKeyTable' },
    { key = 'Space', action = 'PopKeyTable' },
    { key = 'Enter', action = 'PopKeyTable' },
    { key = 'g', mods = 'CTRL', action = 'PopKeyTable' },
  },
}

config.default_prog = { 'nu', '-l' }

return config
