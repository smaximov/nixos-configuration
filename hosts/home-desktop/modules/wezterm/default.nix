{
  config,
  pkgs,
  lib,
  ...
}:

let
  inherit (lib)
    mkIf
    mkEnableOption
    ;

  cfg = config.configs.wezterm;
in
{
  options.configs.wezterm = {
    enable = mkEnableOption "wezterm";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      wezterm
    ];

    xdg.configFile."wezterm/wezterm.lua".source = ./wezterm.lua;
  };
}
