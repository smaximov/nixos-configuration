{ config, pkgs, lib, ... }:

{
  home.packages = with pkgs; [ eza jq ];

  # Force creation of the ZSH cache directory, which stores HISTFILE:
  home.file."${config.xdg.cacheHome}/zsh/.keep".text = "";

  programs.zsh = rec {
    enable = true;

    dotDir = ".config/zsh";

    autosuggestion.enable = true;

    sessionVariables = rec {
      EDITOR = "emacsclient";
      VISUAL = EDITOR;
      PAGER = "${pkgs.less}/bin/less -M";
    };

    shellAliases = {
      e = "nocorrect ${sessionVariables.EDITOR}";
      rm = "rm -v";
      mv = "mv -v";
      cp = "cp -v";
      l = "${pkgs.eza}/bin/eza -l";
      bundle = "noglob bundle";
      rake = "noglob rake";

      "-g L" = "| ${pkgs.less}/bin/less";
      "-g G" = "| ${pkgs.ripgrep}/bin/rg --color=always";
      "-g JQ" = "2>/dev/null | ${pkgs.jq}/bin/jq";
    };

    envExtra = ''
      setopt NO_GLOBAL_RCS
    '';

    history = {
      extended = true;
      ignoreDups = true;
      expireDuplicatesFirst = true;
      path = "${config.xdg.cacheHome}/zsh/zhistory";
    };

    oh-my-zsh = {
      enable = true;

      plugins = [ "git" ];
    };
  };
}
