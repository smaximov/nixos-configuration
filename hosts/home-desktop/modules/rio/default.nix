{
  config,
  lib,
  ...
}:

let
  inherit (lib)
    mkIf
    mkEnableOption
    ;

  inherit (config) configs;

  cfg = configs.rio;
in
{
  options.configs.rio = {
    enable = mkEnableOption "rio";
  };

  config = mkIf cfg.enable {
    stylix.targets.rio.enable = true;

    programs.rio = {
      enable = true;

      settings = {
        confirm-before-quit = false;

        fonts =
          let
            iosevka =
              {
                weight,
                italic ? false,
              }:
              {

                family = "Iosevka";
                stretch = "Normal";
                style = if italic then "Italic" else "Normal";
                inherit weight;
              };
          in
          {
            hinting = true;

            regular = iosevka { weight = 400; };
            bold = iosevka { weight = 800; };

            italic = iosevka {
              weight = 400;
              italic = true;
            };
            bold-italic = iosevka {
              weight = 800;
              italic = true;
            };

            emoji = {
              family = "Noto Color Emoji";
            };
          };

        keyboard.use-kitty-keyboard-protocol = true;
        navigation.mode = "Plain";
        padding-x = 10;

        shell = mkIf configs.nushell.enable {
          program = "nu";
          args = [ "--login" ];
        };

        renderer.performance = "High";
        developer.log-level = "INFO";
      };
    };
  };
}
