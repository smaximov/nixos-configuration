{
  pkgs,
  lib,
  config,
  ...
}:

{
  imports = [
    modules/direnv
    modules/zsh.nix
    modules/tmux
    modules/git
    modules/pgcli
    modules/xdg
    modules/starship
    modules/wezterm
    modules/zoxide.nix
    modules/emacs
    modules/nushell
    modules/paperwm
    modules/gnome
    modules/niri
    modules/rio
    modules/carapace
    modules/firefox
  ];

  home = {
    # This value determines the Home Manager release that your
    # configuration is compatible with. This helps avoid breakage
    # when a new Home Manager release introduces backwards
    # incompatible changes.
    #
    # You can update Home Manager without changing this value. See
    # the Home Manager release notes for a list of state version
    # changes in each release.
    stateVersion = "22.05";

    language.base = "en_US.UTF-8";

    packages =
      with pkgs;
      let
        desktop-apps = [
          tdesktop
          qbittorrent
          keepassxc
          spotify
          mcomix
          mpv
          wally-cli
          thunderbird
          gajim
        ];

        work-tools = [
          kubectl
          kubectx
          pgcli
          ngrok
          rclone
        ];
      in
      [
        shellcheck
        nixfmt-rfc-style
      ]
      ++ lib.optional stdenv.isLinux unar
      ++ desktop-apps
      ++ work-tools;
  };

  programs = {
    jq.enable = true;

    bat.enable = true;
  };

  configs.rio.enable = true;
  configs.starship.enable = true;
  configs.nushell.enable = true;
  configs.carapace.enable = true;
  configs.firefox.enable = true;
}
