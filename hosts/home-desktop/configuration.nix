# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:

rec {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./cachix.nix

    inputs.stylix.nixosModules.stylix
    inputs.home-manager.nixosModules.default
    inputs.ragenix.nixosModules.default

    (
      {
        config,
        pkgs,
        lib,
        inputs,
        ...
      }:
      {
        imports = [ inputs.niri.nixosModules.niri ];
        nixpkgs.overlays = [ inputs.niri.overlays.niri ];

        # Force Chromium/Electron to use Wayland. Not that I use any of them, but having this probably
        # won't hurt ¯\_(ツ)_/¯
        environment.variables.NIXOS_OZONE_WL = "1";

        programs.niri.enable = true;

        fonts.packages = with pkgs.nerd-fonts; [ jetbrains-mono ];
      }
    )

    ({

      stylix =
        let
          theme = "${pkgs.base16-schemes}/share/themes/unikitty-dark.yaml";

          custom-iosevka-font =
            { family, serifs }:
            {
              name = family;
              package = pkgs.iosevka.override {
                set = builtins.replaceStrings [ "Iosevka " " " ] [ "" "" ] family;

                privateBuildPlan = {
                  inherit family serifs;

                  spacing = "quasi-proportional";
                  noCvSs = true;
                  exportGlyphNames = false;
                  noLigation = true;
                };
              };
            };
        in
        {
          enable = true;
          autoEnable = false;
          base16Scheme = theme;
          image = config.lib.stylix.pixel "base01";
          fonts = {
            sizes = {
              terminal = 16;
              popups = 12;
              desktop = 12;
              applications = 16;
            };

            monospace = {
              package = pkgs.iosevka;
              name = "Iosevka";
            };

            sansSerif = custom-iosevka-font {
              family = "Iosevka Custom Sans";
              serifs = "sans";
            };

            serif = custom-iosevka-font {
              family = "Iosevka Custom Serif";
              serifs = "slab";
            };
          };
        };
    })
  ];

  nixpkgs.config.allowUnfree = true;
  nixpkgs.overlays = [
    inputs.emacs-overlay.overlays.default
    inputs.nur.overlays.default
  ];

  nix = {
    extraOptions = ''
      experimental-features = nix-command flakes

      # nix options for derivations to persist garbage collection
      keep-outputs = true
      keep-derivations = true
    '';
  };

  # Use the systemd-boot EFI boot loader.
  boot = {
    tmp = {
      useTmpfs = true;
      tmpfsSize = "75%";
    };

    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };

    kernel.sysctl = {
      "vm.swappiness" = "10";
    };
  };

  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
      LC_TIME = "en_GB.UTF-8";
    };
  };

  # Set your time zone.
  time.timeZone = "Europe/Moscow";

  age = {
    identityPaths = [
      # Backed up in the password manager as "Age Keys/Home Desktop"
      "/home/nameless/.ssh/age_ed25519_key"
    ];
    secrets.wg0-private-key.file = ../../secrets/wg0-private-key.age;

    secrets.v2ray = {
      file = ../../secrets/v2ray/client.age;
      mode = "440";
    };
  };

  environment = {
    gnome.excludePackages = (
      with pkgs;
      [
        cheese # webcam tool
        epiphany # web browser
        geary # email reader
        gnome-photos
        gnome-tour
        yelp
        totem # video player
        gnome-music
        gnome-characters
        tali # poker game
        iagno # go game
        hitori # sudoku game
        atomix # puzzle game
        gnome-contacts
        gnome-initial-setup
      ]
    );

    systemPackages =
      with pkgs;
      let
        modern-cli-tools = [
          fd
          ripgrep
          bottom
          du-dust
          tokei
          hyperfine
          sd
          tealdeer
          xh
          procs
          duf
          dogdns
          gping
          peco
          comby
        ];

        deploy-rs = inputs.deploy-rs.packages.${pkgs.system}.default;

        age-identity-paths-flags = lib.strings.concatMapStringsSep " " (
          path: "-i ${lib.strings.escapeShellArg path}"
        ) config.age.identityPaths;

        rage-wrapper = symlinkJoin {
          name = "rage";
          paths = [ rage ];
          buildInputs = [ makeWrapper ];
          postBuild = ''
            wrapProgram $out/bin/rage \
              --add-flags '${age-identity-paths-flags}'
          '';
        };

        ragenix-wrapper = symlinkJoin {
          name = "ragenix";
          paths = [ inputs.ragenix.packages.${pkgs.system}.default ];
          buildInputs = [ makeWrapper ];
          postBuild = ''
            wrapProgram $out/bin/ragenix \
              --add-flags '${age-identity-paths-flags}'
          '';
        };

        # The upgrade process is:
        #
        # Rebuild nixos configuration with the configuration below added
        # to your configuration.nix. Alternatively, add that into separate
        # file and reference it in imports list.
        #
        # Login as root (`sudo su -`)
        #
        # Run upgrade-pg-cluster. It will stop old postgresql, initialize a
        # new one and migrate the old one to the new one. You may supply
        # arguments like `--jobs 4` and `--link` to speedup migration process.
        # See https://www.postgresql.org/docs/current/pgupgrade.html for details.
        #
        # Change postgresql package in NixOS configuration to the one you were
        # upgrading to via `services.postgresql.package`. Rebuild NixOS. This
        # should start new postgres using upgraded data directory and all
        # services you stopped during the upgrade.
        #
        # After the upgrade it’s advisable to analyze the new cluster.
        #
        # * For PostgreSQL ≥ 14, use the `vacuumdb` command printed by the
        #   upgrades script.
        #
        # * For PostgreSQL < 14, run (as `su -l postgres` in the
        # `services.postgresql.dataDir`, in this example `/var/lib/postgresql/13`):
        #
        #   ```shell
        #   $ ./analyze_new_cluster.sh
        #   ```
        #
        # # Warning
        #
        # The next step removes the old state-directory!
        #
        # ```shell
        # $ ./delete_old_cluster.sh
        # ```
        upgrade-pg-cluster = (
          let
            # Specify the postgresql package you'd like to upgrade to.
            # Do not forget to list the extensions you need.
            newPostgres = pkgs.postgresql_15;
          in
          pkgs.writeScriptBin "upgrade-pg-cluster" ''
            set -eux
            # XXX it's perhaps advisable to stop all services that depend on postgresql
            systemctl stop postgresql

            export NEWDATA="/var/lib/postgresql/${newPostgres.psqlSchema}"

            export NEWBIN="${newPostgres}/bin"

            export OLDDATA="${config.services.postgresql.dataDir}"
            export OLDBIN="${config.services.postgresql.package}/bin"

            install -d -m 0700 -o postgres -g postgres "$NEWDATA"
            cd "$NEWDATA"
            sudo -u postgres $NEWBIN/initdb -D "$NEWDATA"

            sudo -u postgres $NEWBIN/pg_upgrade \
              --old-datadir "$OLDDATA" --new-datadir "$NEWDATA" \
              --old-bindir $OLDBIN --new-bindir $NEWBIN \
              "$@"
          ''
        );
      in
      [
        cachix
        zsh
        duperemove
        nushell
        deploy-rs
        git
        ragenix-wrapper
        rage-wrapper
        upgrade-pg-cluster
      ]
      ++ modern-cli-tools;
  };

  fonts = {
    fontconfig.defaultFonts.monospace = [ "Iosevka" ];

    packages = with pkgs; [
      iosevka
      fira-code
      noto-fonts-color-emoji
    ];
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  programs.zsh.enable = true;

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  networking =
    let
      wireguard-port = 59876;
    in
    {
      # Enables wireless support via wpa_supplicant.
      wireless.enable = false;

      # The global useDHCP flag is deprecated, therefore explicitly set to false here.
      # Per-interface useDHCP will be mandatory in the future, so this generated config
      # replicates the default behaviour.
      useDHCP = false;

      interfaces = {
        enp39s0.useDHCP = true;
        wlp37s0.useDHCP = true;
      };

      # GSConnect
      firewall.allowedUDPPortRanges = [
        {
          from = 1716;
          to = 1764;
        }
      ];

      # Wireguard setup
      firewall.allowedUDPPorts = [ wireguard-port ];

      wg-quick.interfaces.wg0 = {
        autostart = false;
        address = [ "10.8.0.2/24" ];
        privateKeyFile = config.age.secrets.wg0-private-key.path;
        listenPort = wireguard-port;

        peers = [
          {
            publicKey = "NLmlgu8ATDHqWOL9LRCEBzpcMvep2rv4hDWK/T52wDc=";
            # Forward all the traffic via VPN
            allowedIPs = [ "0.0.0.0/0" ];
            endpoint = "vpn.place-holder.ru:58342";
            # Send keepalives every 25 seconds. Important to keep NAT tables alive.
            persistentKeepalive = 25;
          }
        ];
      };
    };

  services.pulseaudio.enable = false;

  virtualisation.podman = {
    enable = true;
    dockerCompat = true;
  };

  services.pipewire = {
    enable = true;

    alsa.enable = true;
    pulse.enable = true;
  };

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;

    xkb.layout = "us,ru";
    xkb.options = "grp:caps_toggle";

    videoDrivers = [ "amdgpu" ];

    displayManager.gdm = {
      enable = true;
    };

    desktopManager.gnome.enable = true;
  };

  services.postgresql = {
    enable = true;
    package = pkgs.postgresql_15;
    ensureUsers = [ { name = "nameless"; } ];
    settings = {
      # DB Version: 15
      # OS Type: linux
      # DB Type: desktop
      # Total Memory (RAM): 32 GB
      # CPUs num: 24
      # Connections num: 100
      # Data Storage: ssd
      max_connections = 100;
      shared_buffers = "2GB";
      effective_cache_size = "8GB";
      maintenance_work_mem = "2GB";
      checkpoint_completion_target = 0.9;
      wal_buffers = "16MB";
      default_statistics_target = 100;
      random_page_cost = 1.1;
      effective_io_concurrency = 200;
      work_mem = "4369kB";
      min_wal_size = "100MB";
      max_wal_size = "2GB";
      max_worker_processes = 24;
      max_parallel_workers_per_gather = 4;
      max_parallel_workers = 24;
      max_parallel_maintenance_workers = 4;
      shared_preload_libraries = [ "pg_stat_statements" ];
    };
  };

  services.syncthing = {
    enable = true;
    openDefaultPorts = true;
    user = "nameless";
    group = "users";
    settings = {
      devices = {
        phone = {
          id = "SEKPMHQ-2VLWFRD-J263ZHN-SGOZCBU-GPSQ5BE-UGR4SFO-AFGPNRP-VUBEZQL";
          name = "realme GT Master Edition";
          introducer = false;
        };
        vps = {
          id = "TNB4VAP-O3LYGF7-KAB2CWQ-TS2LRK3-IIGUNP6-5PNXTLD-LWHIA2J-HML7JAS";
          name = "sync.maximov.space";
          introducer = false;
        };
      };
      folders = {
        books = {
          id = "j7yq2-ztamh";
          label = "Books";
          path = "~/share/sync/books";
          devices = [
            "phone"
            "vps"
          ];
        };
        camera = {
          id = "nexus_5x-photos";
          label = "Camera";
          path = "~/share/sync/camera";
          devices = [
            "phone"
            "vps"
          ];
        };
        gnupg = {
          id = "ddfwi-tojyi";
          label = "GnuPG";
          path = "~/share/sync/gnupg";
          devices = [
            "phone"
            "vps"
          ];
        };
        notes = {
          id = "assid-k4x4i";
          label = "Notes";
          path = "~/share/sync/notes";
          devices = [
            "phone"
            "vps"
          ];
          versioning = {
            type = "staggered";
            params = {
              maxAge = "31536000";
              cleanInterval = "3600";
            };
          };
        };
        secrets = {
          id = "qzihb-vhtm7";
          label = "Secrets";
          path = "~/share/sync/secrets";
          devices = [
            "phone"
            "vps"
          ];
          versioning = {
            type = "simple";
            params.keep = "10";
          };
        };
      };
    };
  };

  systemd.services = {
    create-swapfile = {
      serviceConfig.Type = "oneshot";
      wantedBy = [ "var-swapfile.swap" ];
      script = ''
        swapfile="/var/swapfile"

        if [[ -f "$swapfile" ]]; then
          echo "Swapfile `$swapfile` already exists, skipping"
        else
          echo "Setting up swap file $swapfile"
          ${pkgs.btrfs-progs}/bin/btrfs filesystem mkswapfile --size 64g "$swapfile"
        fi
      '';
    };
  };

  users = {
    mutableUsers = false;

    users = {
      root = {
        hashedPassword = null;
      };

      nameless = {
        isNormalUser = true;
        useDefaultShell = true;
        uid = 1000;
        home = "/home/nameless";

        extraGroups = [
          "wheel" # Enable ‘sudo’ for the user.
          "networkmanager"
          "docker"
          "podman"
        ];

        # Hashed using `mkpasswd --method=sha-512 --rounds=1000000`
        hashedPassword = "$6$rounds=1000000$HLqBEOO7.wf8Te$fXDMOVpJsMPHdlo5nsEuVZJNsGGg6J.LeapEWM32CgWBj8IsqvvvvW3u8fA7e.PHDC6GPZZS7wC/H4F9D/i.x0";
      };
    };
  };

  home-manager.useUserPackages = true;
  home-manager.useGlobalPkgs = true;
  home-manager.backupFileExtension = "backup";
  home-manager.users.nameless = import ./home.nix;

  # HACK: explicitly set `monitors.xml` config to prevent GDM to display
  # login controls on the wrong monitor.
  #
  # See https://discourse.nixos.org/t/gdm-monitor-configuration/6356/4
  systemd.tmpfiles.rules =
    let
      gdm-monitors-config = pkgs.writeText "gdm-monitors.xml" ''
          <monitors version="2">
            <configuration>
              <logicalmonitor>
                <x>0</x>
                <y>0</y>
                <scale>1</scale>
                <primary>yes</primary>
                <monitor>
                  <monitorspec>
                    <connector>DP-1</connector>
                    <vendor>GSM</vendor>
                    <product>LG ULTRAGEAR</product>
                    <serial>201NTABK8192</serial>
                  </monitorspec>
                <mode>
                  <width>2560</width>
                  <height>1440</height>
                  <rate>164.958</rate>
                </mode>
              </monitor>
            </logicalmonitor>
            <disabled>
              <monitorspec>
                <connector>HDMI-1</connector>
                <vendor>SNY</vendor>
                <product>SONY TV  *00</product>
                <serial>0x01010101</serial>
              </monitorspec>
            </disabled>
          </configuration>
        </monitors>
      '';
    in
    lib.mkAfter [ "L+ /run/gdm/.config/monitors.xml - gdm gdm - ${gdm-monitors-config}" ];

  custom.services.v2ray = {
    enable = true;
    secret = config.age.secrets.v2ray;
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "22.05"; # Did you read the comment?
}
