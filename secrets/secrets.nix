let
  public-keys = {
    home-desktop = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMSR0KTRt5zm5sZ0k8QZtgCTINgOOH+zmID7NNCLIxbL";
    # sync.place-holder.ru host key, see `config.services.openssh.hostKeys`
    place-holder-ru = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPpyk+v6ptA7lVD4x34mYmbSoSANKY51NabKKlV2UB7C root@nixos";
  };
in
with public-keys;
{
  "wg0-private-key.age".publicKeys = [ home-desktop ];

  "syncthing-basic-auth.age".publicKeys = [
    home-desktop
    place-holder-ru
  ];

  "gainz.age".publicKeys = [
    home-desktop
    place-holder-ru
  ];

  "v2ray/server.age".publicKeys = [
    home-desktop
    place-holder-ru
  ];

  "v2ray/client.age".publicKeys = [ home-desktop ];
  "v2ray/windows.age".publicKeys = [ home-desktop ];
  "v2ray/windows-foxy.age".publicKeys = [ home-desktop ];
  "v2ray/android.age".publicKeys = [ home-desktop ];
  "v2ray/android-foxy.age".publicKeys = [ home-desktop ];
}
